﻿using System;
using System.Xml;

namespace HSPlayerCore.Lyrics.ALSong
{
    [Serializable]
    public class ALSongLyricsKeyword : ALSongLyrics
    {
        internal ALSongLyricsKeyword(XmlDocument xml)
        {
            OriginXML = xml;
            Result = xml.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText == "true";

            XmlNode node = xml.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[1];
            ID = node.ChildNodes[0].InnerText;
            Title = node.ChildNodes[1].InnerText;
            PureLyrics = node.ChildNodes[2].InnerText;
            Artist = node.ChildNodes[3].InnerText;
            Album = node.ChildNodes[4].InnerText;
            RegisterName = node.ChildNodes[5].InnerText;
            RegisterMail = node.ChildNodes[6].InnerText;
            RegisterURL = node.ChildNodes[7].InnerText;
            RegisterPhone = node.ChildNodes[8].InnerText;
            RegisterComment = node.ChildNodes[9].InnerText;
            ModifierName = node.ChildNodes[10].InnerText;
            ModifierMail = node.ChildNodes[11].InnerText;
            ModifierURL = node.ChildNodes[12].InnerText;
            ModifierPhone = node.ChildNodes[13].InnerText;
            ModifierComment = node.ChildNodes[14].InnerText;

            Author = RegisterName;//string.IsNullOrEmpty(ModifierName) ? RegisterName : ModifierName;
            By = ModifierName;//string.Format("{0}{1}", (RegisterName == null || RegisterName == "") ? "" : RegisterName + " 님 등록", (ModifierName == null || ModifierName == "") ? "" : ", " + ModifierName + " 님 수정");
            if (!string.IsNullOrEmpty(RegisterComment) && !string.IsNullOrEmpty(ModifierComment)) Comment = string.Format("등록자 코멘트: {0} , 수정자 코멘트: {1}", RegisterComment, ModifierComment);
            else if (string.IsNullOrEmpty(RegisterComment)) Comment = RegisterComment;
            else if (string.IsNullOrEmpty(ModifierComment)) Comment = ModifierComment;
        }

        public override object Clone()
        {
            return new ALSongLyricsKeyword((XmlDocument)OriginXML.Clone());
        }
    }
}
