﻿using System;
using System.IO;

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchOGG : SearchHash
    {
        public SearchOGG(string OGGPath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(OGGPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Init(fs);
            }
            finally { if (fs != null) fs.Close(); }
        }
        /// <summary>
        /// 스트림에서 음악의 MD5해쉬 값을 구합니다.
        /// </summary>
        /// <param name="Music">음악 스트림 입니다.</param>
        /// <returns></returns>
        public SearchOGG(Stream OGGStream) { Init(OGGStream); }

        private void Init(Stream OGGStream)
        {
            if (OGGStream == null) MD5Hash = null;
            else
            {
                try
                {
                    if (OGGStream.Position != 0) OGGStream.Position = 0L;
                    byte[] buffer = new byte[0x1b]; //27
                    int read = OGGStream.Read(buffer, 0, buffer.Length);
                    if ((read < buffer.Length) || (OGGStream.Position > 0xc350)) //5000
                    {
                        return;
                    }
                    byte[] MagicNumber = new byte[4];
                    Array.Copy(buffer, 0, MagicNumber, 0, MagicNumber.Length);

                    if (Decoding(MagicNumber, 0, 0) == "OggS")
                    {
                        OGGStream.Position += 4L;
                        b(OGGStream);

                        byte[] buffer5 = new byte[0x28000]; //163,840
                        OGGStream.Read(buffer5, 0, buffer5.Length);

                        MD5Hash = GetHash(buffer5);
                    }
                }
                catch { MD5Hash = null; }
            }
        }

        private static Stream b(Stream stream)
        {
            byte[] numArray = new byte[7];
            int num1 = 0;
            while (stream.Length >= stream.Position)
            {
                stream.Read(numArray, 0, 7);
                byte[] OGG = new byte[7] { 5, 118, 111, 114, 98, 105, 115 };
                if (Decoding(numArray, 0, 0) == Decoding(OGG, 0, 0))
                {
                    long num2 = stream.Position - 7L + 11L;
                    stream.Position = num2;
                    break;
                }
                stream.Position = num1;
                ++num1;
            }
            return stream;
        }
    }
}
