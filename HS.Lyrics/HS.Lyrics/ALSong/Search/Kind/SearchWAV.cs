﻿using System;
using System.IO;

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchWAV : SearchHash
    {
        public SearchWAV(string WAVPath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(WAVPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Init(fs);
            }
            finally { if(fs != null) fs.Close(); }
        }
        /// <summary>
        /// 스트림에서 음악의 MD5해쉬 값을 구합니다.
        /// </summary>
        /// <param name="Music">음악 스트림 입니다.</param>
        /// <returns></returns>
        public SearchWAV(Stream WAVStream) { Init(WAVStream); }

        private void Init(Stream WAVStream)
        {
            if (WAVStream == null) MD5Hash = null;
            else
            {
                try
                {
                    if(WAVStream.Position != 0) WAVStream.Position = 0L;

                    byte[] buffer = new byte[0x28000]; //163,840
                    WAVStream.Read(buffer, 0, buffer.Length);

                    byte[] header = new byte[4];
                    Array.Copy(buffer, header, 4);
                    //if (Decoding(header) == "RIFF") 
                    MD5Hash = GetHash(buffer);
                }
                catch {}
            }
        }
    }
}
