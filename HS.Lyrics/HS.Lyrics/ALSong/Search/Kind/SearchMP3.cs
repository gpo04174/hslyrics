﻿using System;
using System.IO;

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchMP3 : SearchHash
    {
        public SearchMP3(string MP3Path)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(MP3Path, FileMode.Open, FileAccess.Read, FileShare.Read);
                Init(fs);
            }
            finally { if (fs != null) fs.Close(); }
        }
        /// <summary>
        /// 음악 스트림에서 음악의 MD5해쉬 값을 구합니다.
        /// </summary>
        /// <param name="Music">음악 스트림 입니다.</param>
        /// <returns></returns>
        public SearchMP3(Stream MP3Stream) { Init(MP3Stream); }

        private void Init(Stream MP3Stream)
        {
            if (MP3Stream == null) MD5Hash = null;
            else
            {
                try
                {
                    if (MP3Stream.Position != 0) MP3Stream.Position = 0L;
                    byte[] buffer = new byte[0x1b]; //27
                    int read = MP3Stream.Read(buffer, 0, buffer.Length);
                    if ((read < buffer.Length) || (MP3Stream.Position > 0xc350)) //5000
                    {
                        return;
                    }
                    byte[] MagicNumber = new byte[4];
                    Array.Copy(buffer, 0, MagicNumber, 0, MagicNumber.Length);

                    if (Decoding(MagicNumber, 0, 3) == "ID3")
                    {
                        MP3Stream.Position -= 0x11L;
                        byte[] buffer4 = new byte[4];
                        Array.Copy(buffer, 6, buffer4, 0, 4);
                        int num = 0;
                        b1(MP3Stream, ref num, buffer4);
                        //stream = b1(stream);
                        a1(MP3Stream);
                    }
                    else
                    {
                        MP3Stream.Position = 0L;
                        a1(MP3Stream);
                    }
                    byte[] buffer5 = new byte[0x28000]; //163,840
                    MP3Stream.Read(buffer5, 0, buffer5.Length);

                    MD5Hash = GetHash(buffer5);
                }
                catch { MD5Hash = null; }
            }
        }

        private static void a1(Stream stream)
        {
            for (int i = 0; i < 0xc350; i++) //50000
            {
                if ((stream.ReadByte() == 0xff) && ((stream.ReadByte() >> 5) == 7))
                {
                    stream.Position += -2L;
                }
            }
        }
        private static void b1(Stream stream, ref int ReadCount, byte[] A_3)
        {
            ReadCount = 10 + ((((A_3[0] << 21/*0x15*/) | (A_3[1] << 14)) | (A_3[2] << 7)) | A_3[3]);
            if (stream.CanSeek)
            {
                stream.Position = (long)ReadCount;
            }
        }
        private static Stream b1(Stream A_0)
        {
            byte[] buffer = new byte[7];
            for (int i = 0; A_0.Length >= A_0.Position; i++)
            {
                A_0.Read(buffer, 0, 7);
                byte[] buffer2 = new byte[] { 5, 0x76, 0x6f, 0x72, 0x62, 0x69, 0x73 };
                if (Decoding(buffer, 0, 0) == Decoding(buffer2, 0, 0))
                {
                    long num2 = (A_0.Position - 7L) + 11L;
                    A_0.Position = num2;
                    return A_0;
                }
                A_0.Position = i;
            }
            return A_0;
        }
    }
}
