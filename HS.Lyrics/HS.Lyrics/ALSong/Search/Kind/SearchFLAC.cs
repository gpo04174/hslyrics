﻿using System;
using System.IO;

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchFLAC : SearchHash
    {
        public SearchFLAC(string FLACPath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(FLACPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                Init(fs);
            }
            finally { if(fs != null) fs.Close(); }
        }
        /// <summary>
        /// 스트림에서 음악의 MD5해쉬 값을 구합니다.
        /// </summary>
        /// <param name="Music">음악 스트림 입니다.</param>
        /// <returns></returns>
        public SearchFLAC(Stream FLACStream) { Init(FLACStream); }

        private void Init(Stream FLACStream)
        {
            if (FLACStream == null) MD5Hash = null;
            else
            {
                try
                {
                    if (FLACStream.Position != 0) FLACStream.Position = 0L;
                    byte[] buffer = new byte[0x1b]; //27
                    int read = FLACStream.Read(buffer, 0, buffer.Length);
                    if ((read < buffer.Length) || (FLACStream.Position > 0xc350)) //5000
                    {
                        return;
                    }
                    byte[] MagicNumber = new byte[4];
                    Array.Copy(buffer, 0, MagicNumber, 0, MagicNumber.Length);
                    
                    if (Decoding(MagicNumber, 0, 0) == "fLaC")
                    {
                        FLACStream.Position += 4L;
                        CalculateOffset(FLACStream);

                        byte[] buffer5 = new byte[0x28000]; //163,840
                        FLACStream.Read(buffer5, 0, buffer5.Length);

                        MD5Hash = GetHash(buffer5);
                    }
                }
                catch { MD5Hash = null; }
            }
        }
        
        private static Stream CalculateOffset(Stream stream)
        {
            byte[] numArray = new byte[7];
            int num1 = 0;
            while (stream.Length >= stream.Position)
            {
                stream.Read(numArray, 0, 7);
                byte[] VORBIS = new byte[7] {0x05, 0x76, 0x6F, 0x72, 0x62, 0x69, 0x73};
                if (Decoding(numArray, 0, 0) == Decoding(VORBIS, 0, 0))
                {
                    long num2 = stream.Position - 7L + 11L;
                    stream.Position = num2;
                    break;
                }
                stream.Position = num1;
                ++num1;
            }
            return stream;
        }
    }
}
