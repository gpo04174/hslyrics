﻿using System.Security.Cryptography;
using System.Text;

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchHash
    {
        protected SearchHash() { }
        public SearchHash(string MD5Hash) { this.MD5Hash = MD5Hash; }
        public string MD5Hash { get; protected set; }

        public void SetMD5Hash(byte[] Data) { this.MD5Hash = GetHash(Data); }

        public static string GetHash(byte[] Data)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            provider.ComputeHash(Data);
            byte[] hash = provider.Hash;
            StringBuilder builder = new StringBuilder();
            foreach (byte num in hash)
            {
                builder.Append(string.Format("{0:X2}", num));
            }
            return builder.ToString();
        }
        public static string Decoding(byte[] data) { return Encoding.ASCII.GetString(data); }
        public static string Decoding(byte[] data, int index, int count)
        {
            if (count > 0) return Encoding.ASCII.GetString(data, index, count);
            else return Encoding.ASCII.GetString(data);
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(MD5Hash) ? "(Hash is not set)" : MD5Hash;
        }

        public static implicit operator string(SearchHash hash){ return hash == null ? null : hash.MD5Hash; }
    }
}
