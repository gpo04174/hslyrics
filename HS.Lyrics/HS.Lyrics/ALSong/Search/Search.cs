﻿using System;
using System.IO;
using System.Net;
using System.Xml;
using HSPlayerCore.Lyrics.ALSong.Search;
using System.Collections.Generic;
#if NET45
using System.Threading.Tasks;
#else
using System.Threading;
#endif

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public delegate void GetCountAsyncCallback(int Count, Exception ex);
    public delegate void GetKeywordsAsyncCallback(SearchResult[] results, Exception ex);
    public delegate void SearchKeywordAsyncCallback(ALSongLyricsKeyword lyrics, Exception ex);
    public delegate void SearchHashAsyncCallback(ALSongLyricHash lyrics, Exception ex, params object[] param);
    public class Search
    {
        public static readonly string[] SupportExt = { "mp3", "ogg", "flac", "wav", "wma", "m4a", "ape", "mpga", "asf", "cda", "mp4", "mod", "s3m", "fla" };
        public const int DefaultTimeout = 5000;
        private const string XML_Header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:SOAP-ENC=\"http://www.w3.org/2003/05/soap-encoding\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:ns2=\"ALSongWebServer/Service1Soap\" xmlns:ns1=\"ALSongWebServer\" xmlns:ns3=\"ALSongWebServer/Service1Soap12\">";
        private const string URL = "http://lyrics.alsong.co.kr/alsongwebservice/service1.asmx";
        private const string encData = "b28a2fa5adf301df876973d7f5289b8ac9bbb388e91d7d74ed0dbed34b1828c918f76f050dace878537534c9cd4706ea848737d75baf44a4fd39a218722f39c1b6570c83597a8fbf21ef809cb9d9396a9920c188053c6b694272988a711465f5e36abf56fcd8183b0d643290343cdb36ae9ac48f34d36df1af094b7f7039209a";

        private const string XML_GetCount = "<SOAP-ENV:Body><ns1:GetResembleLyric2Count><ns1:stQuery><ns1:strTitle>{0}</ns1:strTitle><ns1:strArtistName>{1}</ns1:strArtistName></ns1:stQuery></ns1:GetResembleLyric2Count></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        public static int GetCount(string Title, string Artist, int Timeout = DefaultTimeout)
        {
            if (string.IsNullOrEmpty(Title) && string.IsNullOrEmpty(Artist)) return -1;
            else
            {
                string Title1 = string.IsNullOrEmpty(Title) ? "" : Title.Replace(">", "&gt;").Replace("<", "&lt;");
                string Artist1 = string.IsNullOrEmpty(Artist) ? "" : Artist.Replace(">", "&gt;").Replace("<", "&lt;");

                string xml_str = string.Format(XML_GetCount, Title1, Artist1);
                XmlDocument xml = Process(XML_Header + xml_str, Timeout);
                return Convert.ToInt32(xml.ChildNodes[1].InnerText);
            }
        }
        public static int GetCount(SearchKeyword data, int Timeout = DefaultTimeout) { return GetCount(data.Title, data.Artist, Timeout); }

        private const string XML_GetKeywords = "<SOAP-ENV:Body><ns1:GetResembleLyricList2><ns1:encData>{0}</ns1:encData><ns1:title>{1}</ns1:title><ns1:artist>{2}</ns1:artist><ns1:pageNo>{3}</ns1:pageNo></ns1:GetResembleLyricList2></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        public static SearchResult[] GetKeywords(string Title, string Artist, int Page = 1, int Timeout = DefaultTimeout)
        {
            if (string.IsNullOrEmpty(Title) && string.IsNullOrEmpty(Artist)) return null;
            else
            {
                string Title1 = string.IsNullOrEmpty(Title) ? "" : Title.Replace(">", "&gt;").Replace("<", "&lt;");
                string Artist1 = string.IsNullOrEmpty(Artist) ? "" : Artist.Replace(">", "&gt;").Replace("<", "&lt;");

                string xml_str = string.Format(XML_GetKeywords, encData, Title1, Artist1, Page);
                XmlDocument xml = Process(XML_Header + xml_str, Timeout);

                XmlNode node_head = xml.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0];
                if(node_head != null)
                {
                    SearchResult[] result = new SearchResult[node_head.ChildNodes.Count];
                    for (int i = 0; i < node_head.ChildNodes.Count; i++)
                    {
                        XmlNode node = node_head.ChildNodes[i];
                        result[i] = new SearchResult(node);
                    }
                    return result;
                }
                return new SearchResult[0];
            }
        }
        public static SearchResult[] GetKeywords(SearchKeyword data, int Page = 1, int Timeout = DefaultTimeout) { return GetKeywords(data.Title, data.Artist, Page, Timeout); }

        private const string XML_SearchKeyword = "<SOAP-ENV:Body><ns1:GetLyricByID2><ns1:encData>{0}</ns1:encData><ns1:lyricID>{1}</ns1:lyricID></ns1:GetLyricByID2></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        public static ALSongLyricsKeyword SearchKeyword(SearchResult Result, int Timeout = DefaultTimeout)
        {
            string xml_str = string.Format(XML_SearchKeyword, encData, Result.ID);
            XmlDocument xml = Process(XML_Header + xml_str, Timeout);

            return new ALSongLyricsKeyword(xml);
        }

        private const string XML_SearchHash = "<SOAP-ENV:Body><ns1:GetLyric7><ns1:encData>{0}</ns1:encData><ns1:stQuery><ns1:strChecksum>{1}</ns1:strChecksum><ns1:strVersion>3.45</ns1:strVersion><ns1:strMACAddress></ns1:strMACAddress><ns1:strIPAddress>192.168.0.1</ns1:strIPAddress></ns1:stQuery></ns1:GetLyric7></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        public static ALSongLyricHash SearchHash(SearchHash hash, int Timeout = DefaultTimeout)
        {
            string xml_str = string.Format(XML_SearchHash, encData, hash.MD5Hash);
            XmlDocument xml = Process(XML_Header + xml_str, Timeout);

            return new ALSongLyricHash(xml, hash.MD5Hash);
        }
        /*
        private const string XML_SearchHash_v5 = "<SOAP-ENV:Body><ns1:GetLyric5><ns1:stQuery><ns1:strChecksum>{0}</ns1:strChecksum><ns1:strVersion>2.0 beta2</ns1:strVersion><ns1:strMACAddress></ns1:strMACAddress><ns1:strIPAddress>255.255.255.0</ns1:strIPAddress></ns1:stQuery></ns1:GetLyric5></SOAP-ENV:Body></SOAP-ENV:Envelope>";
        public static ALSongLyricHash SearchHash_v5(SearchHash data, int Timeout = DefaultTimeout)
        {
            return null;
        }
        */

        #region Async
#if NET45
        public static async Task<int> GetCountAsync(string Title, string Artist, int Timeout = DefaultTimeout) { return await Task.Run(() => { return GetCount(Title, Artist, Timeout); }); }
        public static async Task<int> GetCountAsync(SearchKeyword data, int Timeout = DefaultTimeout) { return await Task.Run(() => { return GetCount(data.Title, data.Artist, Timeout); }); }

        public static async Task<SearchResult[]> GetKeywordsAsync(string Title, string Artist, int Page = 1, int Timeout = DefaultTimeout) { return await Task.Run(() => { return GetKeywordsAsync(Title, Artist, Page, Timeout); }); }
        public static async Task<SearchResult[]> GetKeywordsAsync(SearchKeyword data, int Page = 1, int Timeout = DefaultTimeout) { return await Task.Run(() => { return GetKeywordsAsync(data.Title, data.Artist, Page, Timeout); }); }

        public static async Task<ALSongLyricsKeyword> SearchKeywordAsync(SearchResult Result, int Timeout = DefaultTimeout) { return await Task.Run(() => { return SearchKeyword(Result, Timeout); }); }

        public static async Task<ALSongLyricHash> SearchHashAsync(SearchHash Hash, int Timeout = DefaultTimeout) { return await Task.Run(() => { return SearchHash(Hash, Timeout); }); }
#else
        public static void GetCountAsync(GetCountAsyncCallback Callback, SearchKeyword data, int Timeout = DefaultTimeout) { GetCountAsync(Callback, data.Title, data.Artist, Timeout); }
        public static void GetCountAsync(GetCountAsyncCallback Callback, string Title, string Artist, int Timeout = DefaultTimeout){new Thread(_GetCountAsync).Start(new _GetCountAsyncPack(Callback, Title, Artist, Timeout));}

        public static void GetKeywordsAsync(GetKeywordsAsyncCallback Callback, SearchKeyword data, int Page = 1, int Timeout = DefaultTimeout) { GetKeywordsAsync(Callback, data.Title, data.Artist, Page, Timeout); }
        public static void GetKeywordsAsync(GetKeywordsAsyncCallback Callback, string Title, string Artist, int Page = 1, int Timeout = DefaultTimeout) { new Thread(_GetKeywordsAsync).Start(new _GetKeywordsAsyncPack(Callback, Title, Artist, Page, Timeout)); }

        public static void SearchKeywordAsync(SearchKeywordAsyncCallback Callback, SearchResult Result, int Timeout = DefaultTimeout) { new Thread(_SearchKeywordAsync).Start(new _SearchKeywordAsyncPack(Callback, Result, Timeout)); }

        public static void SearchHashAsync(SearchHashAsyncCallback Callback, SearchHash Hash, int Timeout = DefaultTimeout, params object[] Params) { new Thread(_SearchHashAsync).Start(new _SearchHashAsyncPack(Callback, Hash, Timeout, Params)); }

        #region Runner
        private static void _GetCountAsync(object obj)
        {
            _GetCountAsyncPack pack = (_GetCountAsyncPack)obj;
            try
            {
                int Count = GetCount(pack.Title, pack.Artist, pack.Timeout);
                if (pack.Callback != null) try { pack.Callback.Invoke(Count, null); } catch { }
            }
            catch (Exception ex) { if (pack.Callback != null) try { pack.Callback.Invoke(0, ex); } catch { } }
        }

        private static void _GetKeywordsAsync(object obj)
        {
            _GetKeywordsAsyncPack pack = (_GetKeywordsAsyncPack)obj;
            try
            {
                SearchResult[] results = GetKeywords(pack.Title, pack.Artist, pack.Page, pack.Timeout);
                if (pack.Callback != null) try { pack.Callback.Invoke(results, null); } catch { }
            }
            catch (Exception ex) { if (pack.Callback != null) try { pack.Callback.Invoke(null, ex); } catch { } }
        }

        private static void _SearchKeywordAsync(object obj)
        {
            _SearchKeywordAsyncPack pack = (_SearchKeywordAsyncPack)obj;
            try
            {
                ALSongLyricsKeyword lyrics = SearchKeyword(pack.Result, pack.Timeout);
                if (pack.Callback != null) try { pack.Callback.Invoke(lyrics, null); } catch { }
            }
            catch (Exception ex) { if (pack.Callback != null) try { pack.Callback.Invoke(null, ex); } catch { } }
        }

        private static void _SearchHashAsync(object obj)
        {
            _SearchHashAsyncPack pack = (_SearchHashAsyncPack)obj;
            try
            {
                if (string.IsNullOrEmpty(pack.Hash)) { if (pack.Callback != null) pack.Callback.Invoke(null, null, pack.Params); }
                else
                {
                    ALSongLyricHash lyrics = SearchHash(pack.Hash, pack.Timeout);
                    if (pack.Callback != null) try { pack.Callback.Invoke(lyrics, null, pack.Params); } catch { }
                }
            }
            catch (Exception ex) { if (pack.Callback != null) try { pack.Callback.Invoke(null, ex, pack.Params); } catch { } }
        }
        #endregion

        #region Pack
        internal class _GetCountAsyncPack
        {
            public _GetCountAsyncPack(GetCountAsyncCallback Callback, string Title, string Artist, int Timeout)
            {
                this.Callback = Callback;
                this.Title = Title;
                this.Artist = Artist;
                this.Timeout = Timeout;
            }
            public GetCountAsyncCallback Callback;
            public string Title;
            public string Artist;
            public int Timeout;
        }

        internal class _GetKeywordsAsyncPack
        {
            public _GetKeywordsAsyncPack(GetKeywordsAsyncCallback Callback, string Title, string Artist, int Page, int Timeout)
            {
                this.Callback = Callback;
                this.Title = Title;
                this.Artist = Artist;
                this.Page = Page;
                this.Timeout = Timeout;
            }
            public GetKeywordsAsyncCallback Callback;
            public string Title;
            public string Artist;
            public int Page;
            public int Timeout;
        }

        internal class _SearchKeywordAsyncPack
        {
            public _SearchKeywordAsyncPack(SearchKeywordAsyncCallback Callback, SearchResult Result, int Timeout)
            {
                this.Callback = Callback;
                this.Result = Result;
                this.Timeout = Timeout;
            }
            public SearchKeywordAsyncCallback Callback;
            public SearchResult Result;
            public int Timeout;
        }

        private class _SearchHashAsyncPack
        {
            public _SearchHashAsyncPack(SearchHashAsyncCallback Callback, SearchHash Hash, int Timeout, params object[] Params)
            {
                this.Callback = Callback;
                this.Hash = Hash;
                this.Timeout = Timeout;
                this.Params = Params;
            }
            public SearchHashAsyncCallback Callback;
            public SearchHash Hash;
            public int Timeout;
            public object[] Params;
        }
        #endregion
#endif
        #endregion

        private static XmlDocument Process(string XML, int Timeout = DefaultTimeout)
        {
            WebRequest request = null;
            Stream responseStream = null;
            Stream requestStream = null;
            try
            {
                request = WebRequest.Create(URL);
                request.Method = "POST";
                request.ContentType = "application/soap+xml";
                request.Timeout = Timeout;
                requestStream = request.GetRequestStream();
                StreamWriter writer = new StreamWriter(requestStream);
                writer.WriteLine(XML);
                writer.Close();
                responseStream = request.GetResponse().GetResponseStream();

                XmlDocument document = new XmlDocument();
                document.Load(responseStream);
                responseStream.Close();

                return document;
            }
            /*
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            */
            finally
            {
                if (request != null) requestStream.Close();
                if (responseStream != null) responseStream.Close();
            }
        }
    }

    public class SearchHelper
    {
        public static SearchHash GetHash(string MusicFile)
        {
            int index = MusicFile.LastIndexOf(".");
            if (index > 0)
            {
                string ext = MusicFile.Substring(index + 1).ToLower();
                if (ext == "mp3") return new SearchMP3(MusicFile);
                else if (ext == "ogg") return new SearchOGG(MusicFile);
                else return new SearchWAV(MusicFile);
            }
            else return null;
        }

        public static bool IsSupportExt(string Extension)
        {
            int index = Extension.LastIndexOf(".");
            string ext = index > 0 ? Extension.Substring(index + 1) : Extension;
            for(int i = 0; i < Search.SupportExt.Length; i++)
                if (ext.Trim().ToLower() == Search.SupportExt[i]) return true;
            return false;
        }

        public static SearchResult[] GetKeywords(string Title, string Artist, int Timeout = Search.DefaultTimeout)
        {
            int count = Search.GetCount(Title, Artist, Timeout);

            List<SearchResult> sr = new List<SearchResult>();
            for (int i = 1; i <= Math.Ceiling(count / 100d); i++)
            {
                SearchResult[] sr1 = Search.GetKeywords(Title, Artist, i, Timeout);
                if (sr1 != null && sr1.Length > 0) sr.AddRange(sr1);
            }
            return sr.ToArray();
            /*
            SearchResult[] sr = new SearchResult[count];
            for(int i = 1, j = 0; i <= Math.Ceiling(count / 100d); i++, j+=100)
            {
                SearchResult[] sr1 = Search.GetKeywords(Title, Artist, i, Timeout);
                try { Array.Copy(sr1, 0, sr, j, sr1.Length); } catch { }
            }
            return sr;
            */
        }
        public static SearchResult[] GetKeywords(SearchKeyword data, int Timeout = Search.DefaultTimeout) { return GetKeywords(data.Title, data.Artist, Timeout); }

        #region Async
#if NET45
        public static async Task<SearchResult[]> GetKeywordsAsync(string Title, string Artist, int Timeout = Search.DefaultTimeout) { return await Task.Run(() => { return GetKeywords(Title, Artist, Timeout); }); }
        public static async Task<SearchResult[]> GetKeywordsAsync(SearchKeyword data, int Timeout = Search.DefaultTimeout) { return await Task.Run(() => { return GetKeywords(data.Title, data.Artist, Timeout); }); }
#else
        public static void GetKeywordsAsync(GetKeywordsAsyncCallback Callback, string Title, string Artist, int Timeout = Search.DefaultTimeout) { new Thread(_GetKeywordsAsync).Start(new Search._GetKeywordsAsyncPack(Callback, Title, Artist, 0, Timeout)); }
        public static void GetKeywordsAsync(GetKeywordsAsyncCallback Callback, SearchKeyword data, int Timeout = Search.DefaultTimeout) { new Thread(_GetKeywordsAsync).Start(new Search._GetKeywordsAsyncPack(Callback, data.Title, data.Artist, 0, Timeout)); }

        #region Runner
        private static void _GetKeywordsAsync(object obj)
        {
            Search._GetKeywordsAsyncPack pack = (Search._GetKeywordsAsyncPack)obj;
            try
            {
                SearchResult[] results = GetKeywords(pack.Title, pack.Artist, pack.Timeout);
                if (pack.Callback != null) try { pack.Callback.Invoke(results, null); } catch { }
            }
            catch (Exception ex) { if (pack.Callback != null) try { pack.Callback.Invoke(null, ex); } catch { } }
        }
        #endregion
#endif
#endregion
    }
}
