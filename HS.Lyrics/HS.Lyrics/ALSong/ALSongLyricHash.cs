﻿using HSPlayerCore.Lyrics.ALSong.Search;
using System;
using System.Text;
using System.Xml;

namespace HSPlayerCore.Lyrics.ALSong
{
    /// <summary>
    /// 알송 싱크가사 클래스 입니다.
    /// </summary>
    [Serializable]
    public class ALSongLyricHash : ALSongLyrics
    {
        internal ALSongLyricHash(XmlDocument xml, string MD5Hash)
        {
            OriginXML = xml;
            this.MD5Hash = MD5Hash;

            XmlNode node = xml.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0];
            Result = node.ChildNodes[0].InnerText == "1";
            ID = node.ChildNodes[1].InnerText;
            Title = node.ChildNodes[3].InnerText;
            Artist = node.ChildNodes[4].InnerText;
            Album = node.ChildNodes[5].InnerText;
            PureLyrics = node.ChildNodes[8].InnerText.Replace("<br>", "\r\n");
            RegisterName = node.ChildNodes[9].InnerText;
            RegisterMail = node.ChildNodes[10].InnerText;
            RegisterURL = node.ChildNodes[11].InnerText;
            RegisterPhone = node.ChildNodes[12].InnerText;
            RegisterComment = node.ChildNodes[13].InnerText;
            ModifierName = node.ChildNodes[14].InnerText;
            ModifierMail = node.ChildNodes[15].InnerText;
            ModifierURL = node.ChildNodes[16].InnerText;
            ModifierPhone = node.ChildNodes[17].InnerText;
            ModifierComment = node.ChildNodes[18].InnerText;

            Author = RegisterName;//string.IsNullOrEmpty(ModifierName) ? RegisterName : ModifierName;
            By = ModifierName;//string.Format("{0}{1}", (RegisterName == null || RegisterName == "") ? "" : RegisterName + " 님 등록", (ModifierName == null || ModifierName == "") ? "" : ", " + ModifierName + " 님 수정");
            if (!string.IsNullOrEmpty(RegisterComment) && !string.IsNullOrEmpty(ModifierComment)) Comment = string.Format("등록자 코멘트: {0} , 수정자 코멘트: {1}", RegisterComment, ModifierComment);
            else if (string.IsNullOrEmpty(RegisterComment)) Comment = RegisterComment;
            else if (string.IsNullOrEmpty(ModifierComment)) Comment = ModifierComment;

            try { Date = DateTime.ParseExact(node.ChildNodes[2].InnerText, "yyyy-MM-dd HH:mm:ss.fff", null); } catch { }
        }

        public string MD5Hash { get; private set; }

        public DateTime Date { get; private set; }

        public override object Clone()
        {
            return new ALSongLyricHash((XmlDocument)OriginXML.Clone(), (string)MD5Hash.Clone());
        }
    }
}
