﻿using HSPlayerCore.Lyrics.ALSong.Search;
using System;
using System.Text;
using System.Xml;

namespace HSPlayerCore.Lyrics.ALSong
{
    public abstract class ALSongLyrics : Lyrics, ICloneable
    {
        //internal ALSongLyrics(string ID = null, string Title = null, string Artist = null, string Album = null){ }

        public XmlDocument OriginXML { get; protected set; }

        public string ID { get; protected set; }
        public bool Result { get; protected set; }

        public string RegisterName { get; protected set; }
        public string RegisterMail { get; protected set; }
        public string RegisterURL { get; protected set; }
        public string RegisterPhone { get; protected set; }
        public string RegisterComment { get; protected set; }
        public string ModifierName { get; protected set; }
        public string ModifierMail { get; protected set; }
        public string ModifierURL { get; protected set; }
        public string ModifierPhone { get; protected set; }
        public string ModifierComment { get; protected set; }

        public abstract object Clone();

        //public string ToString(bool MakerFirst = false) { return MakerFirst ? string.Format("{0} > {1} [{2}]", Artist, Title, LyricsMaker) : string.Format("{0} [{1} > {2}]", LyricsMaker, Artist, Title); }


        /// <summary>
        /// 현재 가사 정보를 싱크가사 포맷에 맞게 재구성한 문자열 입니다.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            //ToString(false);
            return base.ToString();
        }

        public static implicit operator SearchResult(ALSongLyrics Instance)
        {
            return new SearchResult(Instance.ID, Instance.Title, Instance.Artist, Instance.Album);
        }
    }
}
