﻿namespace HSPlayerCore.Lyrics
{
    public enum LyricsLanguage
    {
        None,
        /// <summary>
        /// K-POP mode
        /// </summary>
        Korean,
        /// <summary>
        /// I'm not Otaku mode
        /// </summary>
        Japanese,
        /// <summary>
        /// Learn english mode
        /// </summary>
        English
    }
}
