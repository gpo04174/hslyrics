﻿using System;

namespace HSPlayerCore.Lyrics
{
    /// <summary>
    /// 싱크가사 예외 클래스 입니다.
    /// </summary>
    public class LyricsException : Exception
    {
        /// <summary>
        /// 싱크가사 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="InnerExeption">내부의 예외를 설정합니다.</param>
        public LyricsException(Exception InnerExeption = null)
        { this.InnerException = InnerException; _Message = "싱크 가사를 찾을 수 없습니다!"; }
        /// <summary>
        /// 싱크가사 예외 클래스를 초기화 합니다.
        /// </summary>
        /// <param name="Message">표시할 메세지 입니다.</param>
        /// <param name="InnerExeption">내부의 예외를 설정합니다.</param>
        public LyricsException(string Message, Exception InnerExeption = null)
        { this.InnerException = InnerException; _Message = Message; }

        string _Message;
        /// <summary>
        /// 오류를 설명하는 메세지를 가져옵니다.
        /// </summary>
        public override string Message
        {
            get { return _Message; }
        }

        protected new Exception InnerException { get; private set; }
    }
}
