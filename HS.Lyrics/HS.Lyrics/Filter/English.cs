﻿using System.Collections.Generic;

namespace HSPlayerCore.Lyrics.Filter
{
    static class English
    {
        public static List<string> Filter(string[] str)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < str.Length; i++)
            {
                bool Flag = false;
                for (int j = 0; j < str[i].Length; j++)
                {
                    if ((str[i][j] >= 'A' && str[i][j] <= 'Z') ||
                        (str[i][j] >= 'a' && str[i][j] <= 'z')) { Flag = true; break; }

                }
                if (!Flag) list.Add(str[i]);
            }
            return list;
        }
    }
}
