﻿using System;
using System.Collections.Generic;

namespace HSPlayerCore.Lyrics.Filter
{
    static class Korean
    {
        /// <summary>
        /// Split Hangul(한글) to 초성, 중성, 종성
        /// </summary>
        /// <param name="Hangul"></param>
        /// <returns>0 = 초성, 1 = 중성, 2 = 종성</returns>
        public static char[] SplitHangul(char Hangul)
        {
            if ((Hangul >= '가' && Hangul <= '힣') ||
                (Hangul >= 'ㄱ' && Hangul <= 'ㅎ') ||
                (Hangul >= 'ㅏ' && Hangul <= 'ㅣ'))
            {
                char[] c = new char[3];
                int uni = Hangul;
                int 초성 = (uni - 44032) / 588;
                int 종성 = (uni - 44032) % 28;
                c[0] = Convert.ToChar(12593 + 초성 + (초성 < 2 ? 0 : 초성 < 3 ? 1 : 초성 < 6 ? 3 : 초성 < 9 ? 10 : 11));
                c[1] = Convert.ToChar(((uni - 44032) % 588) / 28 + 12623);
                c[2] = 종성 == 0 ? '\0' : Convert.ToChar(12592 + 종성 + (종성 < 8 ? 0 : 종성 < 18 ? 1 : 종성 < 23 ? 2 : 3));
                return c;
            }
            return null;
        }
        public static List<string> Filter(string[] str)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < str.Length; i++)
            {
                bool Flag = false;
                for (int j = 0; j < str[i].Length; j++)
                {
                    if ((str[i][j] >= '가' && str[i][j] <= '힣') ||
                        (str[i][j] >= 'ㄱ' && str[i][j] <= 'ㅎ') ||
                        (str[i][j] >= 'ㅏ' && str[i][j] <= 'ㅣ')) { Flag = true; break; }

                }
                if (!Flag) list.Add(str[i]);
            }
            return list;
        }
    }
}
