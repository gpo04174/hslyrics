﻿using System.Collections.Generic;

namespace HSPlayerCore.Lyrics.Filter
{
    static class Japanese
    {
        public static readonly Dictionary<char, string> Japanese_Hiragana_Full = Japanese_Hiragana_Full_Dic();
        public static readonly Dictionary<char, string> Japanese_Katakana_Full = Japanese_Katakana_Full_Dic();
        public static readonly Dictionary<char, string> Japanese_Katakana_Half = Japanese_Katakana_Half_Dic();
        static Dictionary<char, string> Japanese_Hiragana_Full_Dic()
        {
            Dictionary<char, string> dic = new Dictionary<char, string>();
            dic.Add('あ', null); dic.Add('い', null); dic.Add('う', null); dic.Add('え', null); dic.Add('お', null);
            dic.Add('か', null); dic.Add('き', null); dic.Add('く', null); dic.Add('け', null); dic.Add('こ', null);
            dic.Add('さ', null); dic.Add('し', null); dic.Add('す', null); dic.Add('せ', null); dic.Add('そ', null);
            dic.Add('た', null); dic.Add('ち', null); dic.Add('つ', null); dic.Add('て', null); dic.Add('と', null);
            dic.Add('な', null); dic.Add('に', null); dic.Add('ぬ', null); dic.Add('ね', null); dic.Add('の', null);
            dic.Add('は', null); dic.Add('ひ', null); dic.Add('ふ', null); dic.Add('へ', null); dic.Add('ほ', null);
            dic.Add('ま', null); dic.Add('み', null); dic.Add('む', null); dic.Add('め', null); dic.Add('も', null);
            dic.Add('や', null); dic.Add('ゆ', null); dic.Add('よ', null); dic.Add('ゃ', null); dic.Add('ゅ', null); dic.Add('ょ', null);
            dic.Add('ら', null); dic.Add('り', null); dic.Add('る', null); dic.Add('れ', null); dic.Add('ろ', null);
            dic.Add('わ', null); dic.Add('を', null); dic.Add('ん', null);
            dic.Add('が', null); dic.Add('ぎ', null); dic.Add('ぐ', null); dic.Add('げ', null); dic.Add('ご', null);
            dic.Add('ざ', null); dic.Add('じ', null); dic.Add('ず', null); dic.Add('ぜ', null); dic.Add('ぞ', null);
            dic.Add('だ', null); dic.Add('ぢ', null); dic.Add('づ', null); dic.Add('で', null); dic.Add('ど', null);
            dic.Add('ば', null); dic.Add('び', null); dic.Add('ぶ', null); dic.Add('べ', null); dic.Add('ぼ', null);
            dic.Add('ぱ', null); dic.Add('ぴ', null); dic.Add('ぷ', null); dic.Add('ぺ', null); dic.Add('ぽ', null);
            return dic;
        }
        static Dictionary<char, string> Japanese_Katakana_Full_Dic()
        {
            Dictionary<char, string> dic = new Dictionary<char, string>();
            dic.Add('ア', null); dic.Add('イ', null); dic.Add('ウ', null); dic.Add('エ', null); dic.Add('オ', null);
            dic.Add('カ', null); dic.Add('キ', null); dic.Add('ク', null); dic.Add('ケ', null); dic.Add('コ', null);
            dic.Add('サ', null); dic.Add('シ', null); dic.Add('ス', null); dic.Add('セ', null); dic.Add('ソ', null);
            dic.Add('タ', null); dic.Add('チ', null); dic.Add('ツ', null); dic.Add('テ', null); dic.Add('ト', null);
            dic.Add('ナ', null); dic.Add('ニ', null); dic.Add('ヌ', null); dic.Add('ネ', null); dic.Add('ノ', null);
            dic.Add('ハ', null); dic.Add('ヒ', null); dic.Add('フ', null); dic.Add('ヘ', null); dic.Add('ホ', null);
            dic.Add('マ', null); dic.Add('ミ', null); dic.Add('ム', null); dic.Add('メ', null); dic.Add('モ', null);
            dic.Add('ヤ', null); dic.Add('ユ', null); dic.Add('ヨ', null); dic.Add('ャ', null); dic.Add('ュ', null); dic.Add('ョ', null);
            dic.Add('ラ', null); dic.Add('リ', null); dic.Add('ル', null); dic.Add('レ', null); dic.Add('ロ', null);
            dic.Add('ワ', null); dic.Add('ヲ', null); dic.Add('ン', null);
            dic.Add('ガ', null); dic.Add('ギ', null); dic.Add('グ', null); dic.Add('ゲ', null); dic.Add('ゴ', null);
            dic.Add('ザ', null); dic.Add('ジ', null); dic.Add('ズ', null); dic.Add('ゼ', null); dic.Add('ゾ', null);
            dic.Add('ダ', null); dic.Add('ヂ', null); dic.Add('ヅ', null); dic.Add('デ', null); dic.Add('ド', null);
            dic.Add('バ', null); dic.Add('ビ', null); dic.Add('ブ', null); dic.Add('ベ', null); dic.Add('ボ', null);
            dic.Add('パ', null); dic.Add('ピ', null); dic.Add('プ', null); dic.Add('ペ', null); dic.Add('ポ', null);
            return dic;
        }
        static Dictionary<char, string> Japanese_Katakana_Half_Dic()
        {
            Dictionary<char, string> dic = new Dictionary<char, string>();
            dic.Add('ｱ', null); dic.Add('ｲ', null); dic.Add('ｳ', null); dic.Add('ｴ', null); dic.Add('ｵ', null);
            dic.Add('ｶ', null); dic.Add('ｷ', null); dic.Add('ｸ', null); dic.Add('ｹ', null); dic.Add('ｺ', null);
            dic.Add('ｻ', null); dic.Add('ｼ', null); dic.Add('ｽ', null); dic.Add('ｾ', null); dic.Add('ｿ', null);
            dic.Add('ﾀ', null); dic.Add('ﾁ', null); dic.Add('ﾂ', null); dic.Add('ﾃ', null); dic.Add('ﾄ', null);
            dic.Add('ﾅ', null); dic.Add('ﾆ', null); dic.Add('ﾇ', null); dic.Add('ﾈ', null); dic.Add('ﾉ', null);
            dic.Add('ﾊ', null); dic.Add('ﾋ', null); dic.Add('ﾌ', null); dic.Add('ﾍ', null); dic.Add('ﾎ', null);
            dic.Add('ﾏ', null); dic.Add('ﾐ', null); dic.Add('ﾑ', null); dic.Add('ﾒ', null); dic.Add('ﾓ', null);
            dic.Add('ﾔ', null); dic.Add('ﾕ', null); dic.Add('ﾖ', null); dic.Add('ｬ', null); dic.Add('ｭ', null); dic.Add('ｮ', null);
            dic.Add('ﾗ', null); dic.Add('ﾘ', null); dic.Add('ﾙ', null); dic.Add('ﾚ', null); dic.Add('ﾛ', null);
            dic.Add('ﾜ', null); dic.Add('ｦ', null); dic.Add('ﾝ', null); dic.Add('ﾞ', null); dic.Add('ﾟ', null);
            return dic;
        }

        public static List<string> Filter(string[] str)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < str.Length; i++)
            {
                bool Flag = false;
                for (int j = 0; j < str[i].Length; j++)
                    if (Japanese_Hiragana_Full.ContainsKey(str[i][j]) ||
                       Japanese_Katakana_Full.ContainsKey(str[i][j]) ||
                       Japanese_Katakana_Half.ContainsKey(str[i][j])){ Flag = true; break; }
                if (!Flag) list.Add(str[i]);
            }
            return list;
        }
    }
}
