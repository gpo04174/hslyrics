﻿using System.IO;
using System.Text;

namespace HSPlayerCore.Lyric
{
    /// <summary>
    /// LRC가사 파싱 클래스2 (코드 출처: http://blog.csdn.net/www314599782/article/details/6400034)
    /// </summary>
    public class LyricsParser_1
    {
        public struct TLrc
        {
            public int ms;//毫秒  
            public string lrc;//对应的歌词  
        }

        /// <summary>  
        /// 가사 구조체 표준 배열입니다. (标准的歌词结构数组 )
        /// </summary>  
        public TLrc[] tlrc;
        /// <summary>  
        /// 输入歌词文件路径处理歌词信息  
        /// </summary>  
        /// <param name="FilePath"></param>  
        public LyricsParser_1(string FilePath, Encoding Enc)
        {
            if (Enc == null) Enc = Encoding.Default;
            StreamReader sr = new StreamReader(getLrcFile(FilePath), Enc);
            string[] lrc_1 = sr.ReadToEnd().Split(new char[] { '[', ']' });
            sr.Close();

            format_1(lrc_1);
            format_2(lrc_1);
            format_3();
        }
        public LyricsParser_1(string LyricString)
        {
            string[] lrc_1 = LyricString.Split(new char[] { '[', ']' });

            format_1(lrc_1);
            format_2(lrc_1);
            //format_3();
        }
        /// <summary>  
        /// 格式化不同时间相同字符如“[00:34.52][00:34.53][00:34.54]因为我已经没有力气”  
        /// </summary>  
        /// <param name="lrc_1"></param>  
        private void format_1(string[] lrc_1)
        {
            for (int i = 2, j = 0; i < lrc_1.Length; i += 2, j = i)
            {
                while (lrc_1[j] == string.Empty)
                {
                    lrc_1[i] = lrc_1[j += 2];
                }
            }
        }
        /// <summary>  
        /// 数据添加到结构体  
        /// </summary>  
        /// <param name="lrc_1"></param>  
        private void format_2(string[] lrc_1)
        {
            tlrc = new TLrc[lrc_1.Length / 2];
            for (int i = 1, j = 0; i < lrc_1.Length; i++, j++)
            {
                tlrc[j].ms = timeToMs(lrc_1[i]);
                tlrc[j].lrc = lrc_1[++i];
            }
        }
        /// <summary>  
        /// 时间格式”00:37.61“转毫秒  
        /// </summary>  
        /// <param name="lrc_t"></param>  
        /// <returns></returns>  
        private int timeToMs(string lrc_t)
        {
            float m, s, ms;
            string[] lrc_t_1 = lrc_t.Split(':');
            //这里不能用TryParse如“by:253057646”则有问题  
            try
            {
                m = float.Parse(lrc_t_1[0]);
            }
            catch
            {
                return 0;
            }
            float.TryParse(lrc_t_1[1], out s);
            ms = m * 60000 + s * 1000;
            return (int)ms;
        }
        /// <summary>  
        /// 排序，时间顺序  
        /// </summary>  
        private void format_3()
        {
            TLrc tlrc_temp;
            bool b = true;
            for (int i = 0; i < tlrc.Length - 1; i++, b = true)
            {
                for (int j = 0; j < tlrc.Length - i - 1; j++)
                {
                    if (tlrc[j].ms > tlrc[j + 1].ms)
                    {
                        tlrc_temp = tlrc[j];
                        tlrc[j] = tlrc[j + 1];
                        tlrc[j + 1] = tlrc_temp;
                        b = false;
                    }
                }
                if (b) break;
            }
        }

        public int mark;
        /// <summary>  
        /// 다음 레코드를 읽고, 다음 레코드로 이동 (读取下一条记录,并跳到下一条记录)
        /// </summary>  
        /// <returns></returns>  
        public string ReadLine()
        {
            if (mark < tlrc.Length)
            {
                return tlrc[mark++].lrc;
            }
            else
            {
                return null;
            }

        }
        /// <summary>  
        /// 현재 행의 가사를 읽을 시간 (读取当前行的歌词的时间)
        /// </summary>  
        /// <returns></returns>  
        public int currentTime
        {
            get
            {
                if (mark < tlrc.Length)
                {
                    return tlrc[mark].ms;
                }
                else
                {
                    return -1;
                }
            }
        }

        /// <summary>  
        /// LRC 가사 (현재 디렉토리) 파일 받기 (得到lrc歌词文件(当前目录) )
        /// </summary>  
        /// <param name="file"></param>  
        /// <returns></returns>  
        private string getLrcFile(string file)
        {
            return Path.GetDirectoryName(file) + "//" + Path.GetFileNameWithoutExtension(file) + ".lrc";
        }
    }
}
