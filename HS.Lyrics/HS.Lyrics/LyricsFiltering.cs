﻿using System.Collections.Generic;
using System.Text;
using System;
using HSPlayerCore.Lyrics.Filter;

/// <summary>
/// Copyright by HSKernel (gpo04174)
/// </summary>
namespace HSPlayerCore.Lyrics
{
    public class LyricsFiltering
    {
        public static string ExcludeLanguage(string Lyrics, LyricsLanguage Language)
        {
            if (Language == LyricsLanguage.None) return Lyrics;
            else
            {
                string[] a = Lyrics.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                if (a.Length > 0)
                {
                    List<string> list = null;
                    if (Language == LyricsLanguage.Japanese) list = Japanese.Filter(a);
                    else if (Language == LyricsLanguage.English) list = English.Filter(a);
                    else list = Korean.Filter(a);
                    if (list.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder(list[0]);
                        for (int i = 1; i < list.Count; i++) sb.AppendLine().Append(list[i]);
                        return sb.ToString();
                    }
                }
                return "";
            }
        }


        #region PrivateMember
        /// <summary>
        /// String 배열에서 해당개체가 있는지 검사합니다.
        /// </summary>
        /// <param name="Item">개체를 검사할 배열입니다.</param>
        /// <param name="Match">일치할 개체 입니다.</param>
        /// <param name="IgnoreCapital">대,소문자를 무시할지 여부 입니다.</param>
        /// <returns>개체가 존재하면 true 존재하지 않으면 false를 반환합니다.</returns>
        internal static List<int> IsArrayItem(string[] Item, string Match, bool IgnoreCapital = false)
        {
            List<int> Line = null;
            for (int i = 0; i < Item.LongLength; i++)
            {
                if (!IgnoreCapital)
                {
                    if (Item[i].IndexOf(Match) != -1) { if (Line == null) Line = new List<int>(); Line.Add(i); }
                }
                else if (Item[i].ToLower().IndexOf(Match.ToLower()) != -1) { if (Line == null) Line = new List<int>(); Line.Add(i); }
            }
            return Line;
        }
        #endregion
    }
}
