﻿using System;
using System.Text;

namespace HSPlayerCore.Lyrics
{
    /// <summary>
    /// 싱크가사 추상클래스 입니다.
    /// </summary>
    [Serializable]
    public abstract class Lyrics
    {
        public Lyrics()
        {
            ProgramName = "HS Player";
            ProgramVersion = "3.0";
        }
        public virtual string Artist { get; set; }
        /// <summary>
        /// 앨범 입니다.
        /// </summary>
        public virtual string Album { get; set; }
        /// <summary>
        /// 가사 등록자 입니다.
        /// </summary>
        public virtual string Author { get; set; }
        /// <summary>
        /// 가사 타이틀(제목) 입니다.
        /// </summary>
        public virtual string Title { get; set; }
        /// <summary>
        /// 가사 수정자 입니다.
        /// </summary>
        public virtual string By { get; set; }
        /// <summary>
        /// 노래 길이 입니다.
        /// </summary>
        public virtual string Length { get; set; }
        /// <summary>
        /// 가사 오프셋 입니다. (단위는 밀리세컨드(ms) 입니다.) [※메모: 이 값이 음수면 기존보다 빨리 나타나고 양수면 늦게 나타납니다.]
        /// </summary>
        public virtual int Offset { get; set; }
        /// <summary>
        /// 가사에대한 코멘트(메모)입니다.
        /// </summary>
        public virtual string Comment { get; set; }
        /// <summary>
        /// 순수한 싱크가사만 들어있습니다.
        /// </summary>
        public virtual string PureLyrics { get; set; }
        /// <summary>
        /// 가사를 작성한 프로그램 이름 입니다.
        /// </summary>
        public virtual string ProgramName { get; set; }
        /// <summary>
        /// 가사를 작성한 프로그램 버전 입니다.
        /// </summary>
        public virtual string ProgramVersion { get; set; }
        /// <summary>
        /// 현재 인스턴스가 비었는지 여부를 가져옵니다.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                if ((Artist == null||Artist=="") && (Album == null||Album == "") && (Author == null||Author == "") && (Title == null||Title == "") &&
                    (By==null||By=="")&&(Length == null||Length == "") && (Offset == 0) && (Comment == null||Comment == "")&&(PureLyrics==null||PureLyrics=="")) return true;
                else return false;
            }
        }
        /// <summary>
        /// 인스턴스의 내용을 초기화 합니다.
        /// </summary>
        public virtual void Clear() {Artist = Album = Author = Title = By = Length = PureLyrics = null; Offset = 0; }
        public virtual string GetLyrics(bool IncludeTime)
        {
            return IncludeTime ? ToString() : RemoveSync(PureLyrics);
        }
        /// <summary>
        /// 현재 인스턴스의 내용을 표준 싱크가사 포맷으로 반환합니다.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (Artist != null) sb.AppendFormat("[ar:{0}]\r\n", Artist);
            if (Album != null) sb.AppendFormat("[al:{0}]\r\n", Album);
            if (Author != null) sb.AppendFormat("[au:{0}]\r\n", Author);

            if (Title != null) sb.AppendFormat("[ti:{0}]\r\n", Title);
            if (By != null) sb.AppendFormat("[by:{0}]\r\n", By);
            if (Length != null) sb.AppendFormat("[length:{0}]\r\n", Length);
            if (Offset != 0) sb.AppendFormat("[offset:{0}]\r\n", Offset);
            if (Comment != null) sb.AppendFormat("[comment:{0}]\r\n", Comment);
            if (ProgramName != null) sb.AppendFormat("[re:{0}]\r\n", ProgramName);
            if (ProgramVersion != null) sb.AppendFormat("[ve:{0}]\r\n", ProgramVersion);
            if (PureLyrics != null) sb.Append(PureLyrics);
            return sb.ToString();
        }

        public static string RemoveSync(string Lyric, bool LowMemory = true)
        {
            if (LowMemory)
            {
                StringBuilder sb = new StringBuilder();
                int len = Lyric.Length;

                int start = -1;
                for (int i = 0; i < len; i++)
                {
                    if (start > -1)
                    {
                        if (Lyric[i] == '\n')
                        {
                            string line = Lyric.Substring(start + 1, i - start).Trim();
                            sb.AppendLine(line);
                            start = -1;
                        }
                    }
                    else if (Lyric[i] == '\n') sb.AppendLine();
                    else if (Lyric[i] == ']') start = i;
                }

                return sb.ToString();
            }
            else
            {
                string[] lrc = Lyric.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < lrc.Length; i++)
                {
                    if (lrc[i][0] == '[')
                    {
                        int index = lrc[i].IndexOf("]");
                        if (index > 0)
                            if (sb.Length == 0) sb.Append(lrc[i].Substring(index + 1));
                            else sb.AppendLine(lrc[i].Substring(index + 1));
                    }
                }
                return sb.ToString();
            }
        }
    }
}
