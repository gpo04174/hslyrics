﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HSPlayerCore.Lyrics
{
    [Serializable]
    /// <summary>
    /// LRC 포맷 가사를 나타냅니다 (表示一个LRC格式的歌词) (코드 출처: http://equinox1993.blog.163.com/blog/static/32205137201031141228418/)
    /// </summary>
    public class LyricsManager : Lyrics
    {
        List<string> timeArray = new List<string>();

        Dictionary<string, string> _LyricsDictionary = new Dictionary<string, string>();

        #region values
        LyricsLanguage _ExcludeLanguage;
        public LyricsLanguage ExcludeLanguage { get { return _ExcludeLanguage; } set { _ExcludeLanguage = value; Refresh(); } }

        /// <summary>
        /// Gets the lyrics of the current time. (Refresh() Needs to be called.)<para/>
        /// 현재 시간의 가사를 가져옵니다. (Refresh() 호출이 필요합니다.)<para/>
        /// 返回当前的歌词,使用前请先调用Refresh()函数
        /// </summary>
        public String CurrentLyrics { get; private set; }
        /// <summary>
        /// Gets the next lyrics of the current time.<para/>
        /// 현재 시간의 다음 가사를 가져옵니다.<para/>
        /// 返回下一个歌词,使用前请先调用Refresh()函数
        /// </summary>
        public String NextLyrics { get; private set; }
        /// <summary>
        /// Gets the previous lyrics of the current time. (Refresh() Needs to be called.)<para/>
        /// 현재 시간의 이전 가사를 가져옵니다. (Refresh() 호출이 필요합니다.)<para/>
        /// 返回上一个歌词,使用前请先调用Refresh()函数
        /// </summary>
        public String PreviousLyrics { get; private set; }

        /// <summary>
        /// Gets the original lyrics.<para/>
        /// 원본 가사를 가져옵니다.
        /// </summary>
        public string OriginalLyrics { get; private set; }

        /// <summary>
        /// Gets the current time index.<para/>
        /// 현재 시간의 인덱스를 가져옵니다.<para/>
        /// </summary>
        public int CurrentTimeIndex { get; private set; }

        bool isOffsetEnabled;
        /// <summary>
        /// Determines whether to apply offset. (permanent apply may invoke ApplyOffset().<para/>
        /// 오프셋을 적용할지 결정합니다. (영구적인 적용은 ApplyOffset()을 호출하여주시기 바랍니다.)
        /// </summary>
        public bool OffsetEnabled { get { return isOffsetEnabled; } set { isOffsetEnabled = value; } }

        string _PureLyric;
        public override string PureLyrics
        {
            get {return _PureLyric; }
            set
            {
                SetLyrics(value, true, false);
                _PureLyric = base.PureLyrics = value;
            }
        }
        #endregion

        #region Initalizer
        /// <summary>
        /// 初始化新的LyricParser实例
        /// </summary>
        public LyricsManager()
        {
            OffsetEnabled = true;
            CurrentTimeIndex = 0;
        }
        /// <summary>
        /// 通过指定的Lrc文件初始化LyricParser实例
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <param name="enc">文件编码</param>
        public LyricsManager(string filePath, Encoding enc, bool Mapping_Time_0)
        {
            isOffsetEnabled = true;
            CurrentTimeIndex = 0;
            SetLyrics(File.ReadAllText(filePath, enc), Mapping_Time_0);
        }
        /// <summary>
        ///  通过指定的Lrc代码初始化LyricParser实例
        /// </summary>
        /// <param name="code">Lrc代码</param>
        public LyricsManager(string code, bool Mapping_Time_0)
        {
            isOffsetEnabled = true;
            CurrentTimeIndex = 0;
            SetLyrics(code, Mapping_Time_0);
        }
        public LyricsManager(Lyrics Lyrics) { SetLyrics(Lyrics); }
        public LyricsManager(LyricsManager Manager) { SetLyrics(Manager); }
        #endregion

        #region protected functions
        protected static void Parse(LyricsManager Manager, string Lyrics, bool Mapping_Time_0)
        {
            if (Lyrics == null) return;
            Dictionary<string, string> md = new Dictionary<string, string>();

            string[] lrc = Lyrics.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> time = new List<string>();
            string before_time = null;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lrc.Length; i++)
            {
                if (!string.IsNullOrEmpty(lrc[i]))
                {
                    if (lrc[i].IndexOf("ti:") == 1) Manager.Title = lrc[i].Substring(4, lrc[i].Length - 5);// SetDic(md, "ti", lrc[i].Substring(4, lrc[i].Length - 5));
                    else if(lrc[i].IndexOf("ar:") == 1) Manager.Artist = lrc[i].Substring(4, lrc[i].Length - 5);//SetDic(md, "ar", lrc[i].Substring(4, lrc[i].Length - 5));
                    else if (lrc[i].IndexOf("al:") == 1) Manager.Album = lrc[i].Substring(4, lrc[i].Length - 5); //SetDic(md, "al", lrc[i].Substring(4, lrc[i].Length - 5));
                    else if (lrc[i].IndexOf("au:") == 1) Manager.Author = lrc[i].Substring(4, lrc[i].Length - 5);//SetDic(md, "au", lrc[i].Substring(4, lrc[i].Length - 5));
                    else if (lrc[i].IndexOf("by:") == 1) Manager.By = lrc[i].Substring(4, lrc[i].Length - 5);//SetDic(md, "by", lrc[i].Substring(4, lrc[i].Length - 5));
                    else if (lrc[i].IndexOf("comment:") == 1) Manager.Comment = lrc[i].Substring(9, lrc[i].Length - 10); //SetDic(md, "comment", lrc[i].Substring(9, lrc[i].Length - 10));
                    else if (lrc[i].IndexOf("length:") == 1) Manager.Length = lrc[i].Substring(8, lrc[i].Length - 9);//SetDic(md, "length", lrc[i].Substring(8, lrc[i].Length - 9));
                    else if (lrc[i].IndexOf("offset:") == 1) //SetDic(md, "offset", lrc[i].Substring(8));
                    {
                        int offset;
                        int.TryParse(lrc[i].Substring(8, lrc[i].Length - 9), out offset);
                        Manager.Offset = offset;
                    }
                    else
                    {
                        string[] line = SplitTime(lrc[i]);

                        if (line.Length == 2)
                        {
                            if (line[0] == "00:00.00")
                            {
                                if (Mapping_Time_0 && !string.IsNullOrEmpty(line[1]))
                                {
                                    if (string.IsNullOrEmpty(before_time)) before_time = "00:00.00";
                                    sb.AppendFormat("[{0}]", before_time).AppendLine(line[1]);
                                    SetDic(md, before_time, line[1], true);
                                }
                                else sb.AppendLine(lrc[i]);
                            }
                            else
                            {
                                sb.AppendLine(lrc[i]);
                                before_time = line[0];
                                if (!md.ContainsKey(before_time)) time.Add(before_time);
                                SetDic(md, before_time, line[1], true);
                            }
                            /*
                            if (Mapping_Time_0 && line[0] == "00:00.00" && !string.IsNullOrEmpty(line[1]))
                                SetDic(md, before_time, line[1], true);
                            else
                            {
                                before_time = line[0];
                                if (!md.ContainsKey(before_time)) time.Add(before_time);
                                SetDic(md, before_time, line[1], true);
                            }
                             */
                        }
                        else sb.AppendLine(lrc[i]);
                    }
                }
            }
            Manager._PureLyric = sb.ToString();
            time.Sort();
            if (md.ContainsKey("00:00.00")) md["00:00.00"] = md["00:00.00"].Trim();
            if (time.Count > 0 && time[0] == "00:00.00") time.RemoveAt(0);
            Manager.timeArray = time;
            Manager._LyricsDictionary = md;
        }

        protected static string[] SplitTime(string Lyric)
        {
            if (Lyric[0] == '[')
            {
                int index = Lyric.IndexOf(']');
                string[] ar = new string[2];
                ar[0] = Lyric.Substring(1, index - 1);
                ar[1] = Lyric.Substring(index + 1);
                return ar;
            }
            else return new string[] { Lyric };
        }

        protected static void SetDic(Dictionary<string, string> dic, string Key, string Value, bool Time = false)
        {
            if (string.IsNullOrEmpty(Key)) Key = "";

            if (!Time && Value[Value.Length - 1] == ']') Value = Value.Remove(Value.Length - 1);

            if (!dic.ContainsKey(Key)) dic.Add(Key, Value);
            else if (Time) dic[Key] += ("\r\n" + Value);
            else { dic[Key] = Value; }
        }

        protected static List<string> SortKeys(Dictionary<string, string> dictionary)
        {
            List<string> a = new List<string>(dictionary.Keys);
            a.Sort();
            if (a.Count > 0 && a[0] == "00:00.00") a.RemoveAt(0);
            return a;
        }
        
        protected static String intervalToString(double interval)
        {
            int min;
            float sec;
            min = (int)interval / 60;
            sec = (float)(interval - (float)min * 60.0);
            String smin = String.Format("{0:d2}", min);
            String ssec = String.Format("{0:00.00}", sec);
            return smin + ":" + ssec;
        }
        protected static double stringToInterval(String str)
        {
            string[] a = str.Split(':');
            if (a.Length > 1)
            {
                double min = 0, sec = 0;
                bool parse = double.TryParse(a[0].ToString(), out min);
                parse |= double.TryParse(a[1].ToString(), out sec);
                return parse ? min * 60.0 + sec : uint.MaxValue;
            }
            else return uint.MaxValue;
        }
        #endregion

        #region Method
        public override void Clear()
        {
            Artist = Album = Author = Title = By = Length = _PureLyric = null;
            Offset = 0;
            OffsetEnabled = true;
            _LyricsDictionary.Clear();
            timeArray.Clear();
        }

        /// <summary>
        /// LRC 코드를 설정 합니다. (获取或设置LRC歌词代码)
        /// </summary>
        public void SetLyrics(string Lyrics, bool Mapping_Time_0, bool Clear = true)
        {
            if (!string.IsNullOrEmpty(Lyrics)) { if (Clear) this.Clear(); Parse(this, Lyrics, Mapping_Time_0); }
            else this.Clear();
        }
        public void SetLyrics(Lyrics Lyrics)
        {
            if (Lyrics == null) Clear();
            else
            {
                Title = Lyrics.Title;
                Artist = Lyrics.Artist;
                Album = Lyrics.Album;
                Author = Lyrics.Author;
                Comment = Lyrics.Comment;
                Length = Lyrics.Length;
                Offset = Lyrics.Offset;
                this.PureLyrics = Lyrics.PureLyrics;
            }
        }
        public void SetLyrics(LyricsManager Manager)
        {
            if (Manager == null) Clear();
            else
            {
                Title = Manager.Title;
                Artist = Manager.Artist;
                Album = Manager.Album;
                Author = Manager.Author;
                Comment = Manager.Comment;
                Length = Manager.Length;
                Offset = Manager.Offset;
                OffsetEnabled = Manager.OffsetEnabled;
                _PureLyric = Manager._PureLyric;
                _LyricsDictionary = Manager._LyricsDictionary;
                timeArray = Manager.timeArray;

                CurrentLyrics = Manager.CurrentLyrics;
                NextLyrics = Manager.NextLyrics;
                PreviousLyrics = Manager.PreviousLyrics;
                CurrentTimeIndex = Manager.CurrentTimeIndex;
            }
        }

        /// <summary>
        /// Remapping time from offset. (Millisecond)
        /// </summary>
        public void ApplyOffset(int Offset) { ApplyOffset(Offset / 1000d); }
        /// <summary>
        /// Remapping time from offset. (Second.Millisecond)
        /// </summary>
        public void ApplyOffset(double Offset)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            /*
            if (_LyricsDictionary.ContainsKey("ti")) dic.Add("ti", _LyricsDictionary["ti"]);
            if (_LyricsDictionary.ContainsKey("ar")) dic.Add("ar", _LyricsDictionary["ar"]);
            if (_LyricsDictionary.ContainsKey("by")) dic.Add("by", _LyricsDictionary["by"]);
            if (_LyricsDictionary.ContainsKey("al")) dic.Add("al", _LyricsDictionary["al"]);
            if (_LyricsDictionary.ContainsKey("au")) dic.Add("au", _LyricsDictionary["au"]);
            if (_LyricsDictionary.ContainsKey("length")) dic.Add("length", _LyricsDictionary["length"]);
            if (_LyricsDictionary.ContainsKey("comment")) dic.Add("comment", _LyricsDictionary["comment"]);
            if (_LyricsDictionary.ContainsKey("offset")) dic.Add("offset", _LyricsDictionary["offset"]);
            */
            for (int i = 0; i < timeArray.Count; i++)
            {
                string tmp = _LyricsDictionary[timeArray[i]];
                double time = stringToInterval(timeArray[i]) + Offset;
                dic.Add(intervalToString(time), tmp);
            }
            _LyricsDictionary = dic;
            timeArray = SortKeys(dic);
            PureLyrics = ApplyOffset(PureLyrics, Offset);
        }

        #region refresh functions
        double _Second;
        /// <summary>
        /// 현재 가사를 새로 지정된 시간의 예 (使用指定的时间刷新实例的当前歌词)
        /// </summary>
        /// <param name="Second">시간 (时间)</param>
        public void Refresh(double Second)
        {
            try
            {
                _Second = Second;
                CurrentLyrics = null;
                if (timeArray.Count > 0  && (Second - (double)Offset / 1000.0 >= stringToInterval(timeArray[CurrentTimeIndex].ToString()) && Second - (double)Offset / 1000.0 < stringToInterval(timeArray[CurrentTimeIndex + 1].ToString())))
                {
                    CurrentLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex]]);
                    if (CurrentTimeIndex + 1 < timeArray.Count)
                        NextLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex + 1]]);
                    if (CurrentTimeIndex - 1 >= 0)
                        PreviousLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex - 1]]);
                }
                else if (timeArray.Count > 0 && (Second - (double)Offset / 1000.0 >= stringToInterval(timeArray[CurrentTimeIndex + 1].ToString()) && Second - (double)Offset / 1000.0 < stringToInterval(timeArray[CurrentTimeIndex + 2].ToString())))
                {
                    CurrentTimeIndex++;
                    CurrentLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex]]);
                    if (CurrentTimeIndex + 1 < timeArray.Count)
                        NextLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex + 1]]);
                    if (CurrentTimeIndex - 1 >= 0)
                        PreviousLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex - 1]]);
                }
                else
                {
                    int i;
                    for (i = 0; i < timeArray.Count; i++)
                    {
                        try
                        {
                            if (Second - (double)Offset / 1000.0 >= stringToInterval(timeArray[i].ToString()) && Second - (double)Offset / 1000.0 < stringToInterval(timeArray[i + 1].ToString()))
                            {
                                CurrentTimeIndex = i;
                                CurrentLyrics = Filter(_LyricsDictionary[timeArray[i]]);
                                if (i + 1 < timeArray.Count)
                                    Filter(NextLyrics = _LyricsDictionary[timeArray[i + 1]]);
                                if (i - 1 >= 0)
                                break;
                            }
                        }
                        catch { break; }
                    }
                }
            }
            catch(ArgumentOutOfRangeException ex)
            {
                try
                {
                    CurrentLyrics = Filter(_LyricsDictionary[timeArray[CurrentTimeIndex + 1]]);
                    NextLyrics = "";
                    if (CurrentTimeIndex >= 0)
                        Filter(PreviousLyrics = _LyricsDictionary[timeArray[CurrentTimeIndex - 1]]);
                }
                catch { }
            }
        }
        /// <summary>
        /// 지정된 시간 문자열 현재 인스턴스 가사의 형식을 새로 고침 (使用指定的时间字符串刷新实例的当前歌词,格式为)"mm:ss.ss"
        /// </summary>
        /// <param name="aString">시간 문자열 (时间字符串)</param>
        public void Refresh(String aString)
        {
            Refresh(stringToInterval(aString));
        }
        private void Refresh() { Refresh(_Second); }

        private string Filter(string lrc) { return LyricsFiltering.ExcludeLanguage(lrc, ExcludeLanguage); }
        #endregion

        #region directly get lyrics functions
        /// <summary>
        /// 연결 안가 null을 반환하는 경우, 시간에 주어진 지점의 가사에 직접 액세스 (直接获取指定时间点的歌词,若没有此时间点则返回null)
        /// </summary>
        /// <param name="time">시간 (时间点)</param>
        /// <returns>가사 (歌词)</returns>
        public String Lyric(double time)
        {
            try
            {
                return _LyricsDictionary[intervalToString(time - (double)Offset / 1000.0)].ToString();
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 시간 문자열 가사의 지정된 지점에 직접 액세스, 그렇지 않은 경우이 시간은 NULL을 반환 (直接获取指定时间点字符串的歌词,若没有此时间点则返回null)
        /// </summary>
        /// <param name="aString">시간 문자열 (时间点字符串)</param>
        /// <returns>가사 (歌词)</returns>
        public String Lyric(String aString)
        {
            try
            {
                return _LyricsDictionary[intervalToString(stringToInterval(aString) - (double)Offset / 1000.0)].ToString();
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 지정된 인덱스의 가사를 가져옵니다 (获取指定索引的歌词)
        /// </summary>
        /// <param name="index">인덱스  입니다. (索引)</param>
        /// <returns>가사 (歌词)</returns>
        public String LyricsAtIndex(int index) { return _LyricsDictionary.ContainsKey(timeArray[index]) ? _LyricsDictionary[timeArray[index]] : null; }
        #endregion
        #endregion

        #region override
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("[ti:{0}]", Title).AppendLine();
            sb.AppendFormat("[ar:{0}]", Artist).AppendLine();
            sb.AppendFormat("[al:{0}]", Album).AppendLine();
            sb.AppendFormat("[au:{0}]", Author).AppendLine();
            sb.AppendFormat("[by:{0}]", By).AppendLine();
            sb.AppendFormat("[comment:{0}]", Comment).AppendLine();
            sb.AppendFormat("[length:{0}]", Length).AppendLine();
            sb.AppendFormat("[offset:{0}]", Offset).AppendLine();
            sb.Append(PureLyrics);
            /*
            foreach (KeyValuePair<string,string> key in _lyricsDictionary)
            {
                if (key.Key.ToLower() == "ti" ||
                    key.Key.ToLower() == "ar" ||
                    key.Key.ToLower() == "by" ||
                    key.Key.ToLower() == "al" ||
                    key.Key.ToLower() == "au" ||
                    key.Key.ToLower() == "length" ||
                    key.Key.ToLower() == "comment"||
                    key.Key.ToLower() == "offset")
                    sb.AppendFormat("[{0}:{1}]\r\n", key.Key, key.Value);
                else
                {
                    if (key.Value != null)
                    {
                        string[] a = key.Value.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < a.Length; i++) sb.AppendFormat("[{0}]{1}", key.Key, a[i]).AppendLine();
                    }
                    else sb.AppendFormat("[{0}]", key.Key).AppendLine();
                }
            }
            */
            return sb.ToString();
        }

        Array CopyforICollection(ICollection<string> Collection)
        {
            string[] o = new string[Collection.Count];
            Collection.CopyTo(o, 0);
            return o;
        }
        string[] ICollectionToStringArray(ICollection Collection)
        {
            string[] o = new string[Collection.Count];
            Collection.CopyTo(o, 0);
            return o;
        }
        #endregion

        public static string ApplyOffset(string Lyrics, int Offset) { return ApplyOffset(Lyrics, Offset / 1000d); }
        public static string ApplyOffset(string Lyrics, double Offset)
        {
            string[] a = Lyrics.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i][0] == '[')
                {
                    if (a[i].IndexOf("ti:") == 1 ||
                        a[i].IndexOf("ar:") == 1 ||
                        a[i].IndexOf("by:") == 1 ||
                        a[i].IndexOf("al:") == 1 ||
                        a[i].IndexOf("au:") == 1 ||
                        a[i].IndexOf("length:") == 1 ||
                        a[i].IndexOf("comment:") == 1 ||
                        a[i].IndexOf("offset:") == 1) sb.AppendLine(a[i]);
                    else
                    {
                        int index = a[i].IndexOf(']');
                        if (index > 0)
                        {
                            double d = stringToInterval(a[i].Substring(1, index - 1));
                            if (d == 0 || d == uint.MaxValue) sb.AppendLine(a[i]);
                            else { d += Offset; sb.AppendFormat("[{0}{1}", intervalToString(d), a[i].Substring(index)).AppendLine(); }
                        }
                        else sb.AppendLine(a[i]);
                    }
                }
                else sb.AppendLine(a[i]);
            }
            return sb.ToString();
        }
    }
}
