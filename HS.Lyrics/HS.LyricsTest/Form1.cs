﻿using HSPlayerCore.Lyrics.ALSong;
using HSPlayerCore.Lyrics.ALSong.Search;
using System;
using System.Windows.Forms;

namespace HS.LyricsTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Search.GetCount(new SearchKeyword(textBox1.Text, textBox2.Text)).ToString(), "");
        }

        SearchResult[] results;
        private void button2_Click(object sender, EventArgs e)
        {
            results = Search.GetKeywords(textBox1.Text, textBox2.Text);
            MessageBox.Show(results[0].ID, "");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ALSongLyricsKeyword sr = Search.SearchKeyword(results[0]);
            MessageBox.Show(sr.ToString(), "");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                string hash = SearchHelper.GetHash(openFileDialog1.FileName);
                MessageBox.Show(hash, "MD5Hash");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SearchHash hash = new SearchMP3(openFileDialog1.FileName);
                ALSongLyricHash lrc = Search.SearchHash(hash);
                MessageBox.Show(lrc.Title, "");
            }
        }
    }
}
